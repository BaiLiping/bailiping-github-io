---
layout: page
title: Radar System
---

- [Lincoln Laboratory: Introductory Level](https://www.ll.mit.edu/outreach/radar-introduction-radar-systems-online-course)
- [Lincoln Laboratory: Graduate Level](http://radar-course.org/radar%20se%20List%20of%20Lectures%20.html)
- <iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLNKgf4Cv_NZ1_fZt0bAJXAro6MlaTAEAU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# Lecture Slides
<embed src="/assets/docs/RadarSystem/Overview.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/RadarSystem/RadarBook.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/RadarSystem/RadarBook2.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/RadarSystem/RadarSystem01.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/RadarSystem/RadarSystem02.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/RadarSystem/RadarSystem03.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/RadarSystem/RadarSystem04.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/RadarSystem/RadarSystem05.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/RadarSystem/RadarSystem06.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/RadarSystem/RadarSystem07.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/RadarSystem/RadarSystem08.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/RadarSystem/RadarSystem09.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/RadarSystem/RadarSystem10.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/RadarSystem/RadarSystem11.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/RadarSystem/review.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/RadarSystem/SensingOverview.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/RadarSystem/Sensors_InfoProc-01.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/RadarSystem/Sensors_InfoProc-02.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/RadarSystem/Sensors_InfoProc-03.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/RadarSystem/Sensors_InfoProc-04.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/RadarSystem/Sensors_InfoProc-05.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/RadarSystem/Sensors_InfoProc-06.pdf" type="application/pdf" width="100%" height=1000>

### lecture series
<iframe  width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLJAlx-5DOdeMNjpg4sRO6cty3gL_PZeCE" title="TI Training Material" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<iframe  width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLJAlx-5DOdePomvfyvcM5mrmgxptfYPoa" title="mmWave Sensing" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<iframe  width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLyqL4-20ZuTTlo9of_1lpVYvf_9UVk-DL" title="Synthetic Aperture Radar" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<iframe  width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLUJAYadtuizA8RC2Qk8LfmiWA56HZsk9y" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<iframe  width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLyqL4-20ZuTTDSyf0-aMpgKTwUB1dkfeY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<iframe  width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLUJAYadtuizA5JblMkTYUuOhUqiQco0pu" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### NOTE
- ![SAR](/assets/img/Radar/SAR.png)
- ![scatter](/assets/img/Radar/scatter.png)
- ![rangefromphase](/assets/img/Radar/Rangefromphase.png)
<embed src="/assets/docs/Radar/radar10.pdf" type="application/pdf" width="100%" height=1000>

- ![1](/assets/img/Radar/attenuation.png)
- ![1](/assets/img/Radar/gain.png)
- ![1](/assets/img/Radar/automotiveradar.png)
- ![1](/assets/img/Radar/falsealarm.png)
- ![1](/assets/img/Radar/ambiguity.png)
- ![1](/assets/img/Radar/cassegrain.png)
- ![1](/assets/img/Radar/cfar.png)
- ![1](/assets/img/Radar/clutter.png)
- ![1](/assets/img/Radar/p_d.png)
- ![1](/assets/img/Radar/rcs1.png)
- ![1](/assets/img/Radar/rcs2.png)
- ![1](/assets/img/Radar/all_weather1.png)
- ![1](/assets/img/Radar/all_weather2.png)