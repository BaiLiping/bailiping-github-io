---
layout: page
title: Linear System
---
[EE263](http://ee263.stanford.edu/)
<embed src="/assets/docs/LinearSystem/ee263.pdf" type="application/pdf" width="100%" height=1000>

[EE363](https://stanford.edu/class/ee363/lectures.html)

<embed src="/assets/docs/LinearSystem/book.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/LinearSystem/book2.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/LinearSystem/1.pdf" type="application/pdf" width="100%" height=480>
<embed src="/assets/docs/LinearSystem/2.pdf" type="application/pdf" width="100%" height=480>
<embed src="/assets/docs/LinearSystem/3.pdf" type="application/pdf" width="100%" height=480>
<embed src="/assets/docs/LinearSystem/assignment1.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/LinearSystem/4.pdf" type="application/pdf" width="100%" height=480>
<embed src="/assets/docs/LinearSystem/5.pdf" type="application/pdf" width="100%" height=480>
<embed src="/assets/docs/LinearSystem/6.pdf" type="application/pdf" width="100%" height=480>
<embed src="/assets/docs/LinearSystem/7.pdf" type="application/pdf" width="100%" height=480>
<embed src="/assets/docs/LinearSystem/8.pdf" type="application/pdf" width="100%" height=480>
<embed src="/assets/docs/LinearSystem/9.pdf" type="application/pdf" width="100%" height=480>
<embed src="/assets/docs/LinearSystem/assignment2.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/LinearSystem/10.pdf" type="application/pdf" width="100%" height=480>
<embed src="/assets/docs/LinearSystem/assignment4.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/LinearSystem/11.pdf" type="application/pdf" width="100%" height=480>
<embed src="/assets/docs/LinearSystem/12.pdf" type="application/pdf" width="100%" height=480>
<embed src="/assets/docs/LinearSystem/assignment5.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/LinearSystem/13.pdf" type="application/pdf" width="100%" height=480>
<embed src="/assets/docs/LinearSystem/14.pdf" type="application/pdf" width="100%" height=480>
<embed src="/assets/docs/LinearSystem/15.pdf" type="application/pdf" width="100%" height=480>
<embed src="/assets/docs/LinearSystem/16.pdf" type="application/pdf" width="100%" height=480>
<embed src="/assets/docs/LinearSystem/assignment6.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/LinearSystem/assignment7.pdf" type="application/pdf" width="100%" height=1000>

第三章的最小实现是必考题，大家一定要掌握；第四章的极点配置会结合后面知识点一起考,这个15分就是BIBO BIBS的分析，认真做,基本是计算题（60-70）和概念题（20左右？），证明题应该就一道，是课上讲过的