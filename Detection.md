---
layout: page
title: Radar Based Object Detection
---

----
- TOC
{:toc}
----

### object detection overview
![1](/assets/img/radarproject/projectoverview/objectdetection_onestep.png)
![1](/assets/img/radarproject/projectoverview/objectdetection_twosteps.png)
![1](/assets/img/radarproject/projectoverview/objectdetection_static.png)

### object detection current solution
![1](/assets/img/radarproject/projectoverview/objectdetection_currentsolution1.png)
![1](/assets/img/radarproject/projectoverview/objectdetection_currentsolution2.png)

### repositories
- [Radar_PointNet_InstanceSegmentation](https://github.com/chisyliu/Radar_PointNet_InstanceSegmentation)
- [ScaledYOLO](https://github.com/WongKinYiu/ScaledYOLOv4)
- [YOLO-v4](https://github.com/WongKinYiu/PyTorch_YOLOv4)


### lecture series
<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PL_IHmaMAvkVxdDOBRg2CbcJBq9SY7ZUvs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
[YOLOv4](https://www.bilibili.com/video/BV1Q54y1D7vj)
<iframe width="560" height="315" src="//player.bilibili.com/player.html?aid=840800367&bvid=BV1Q54y1D7vj&cid=197256538&page=1" frameborder="0" allowfullscreen> </iframe>

### important papers
<embed src="/assets/docs/ObjectDetection/PointNet.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ObjectDetection/YOLOv4.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ObjectDetection/ScaledYOLO.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ObjectDetection/ObjectDetection1.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ObjectDetection/ObjectDetection2.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ObjectDetection/ObjectDetection3.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ObjectDetection/2d.pdf" type="application/pdf" width="100%" height=1000>

### tricks
<embed src="/assets/docs/ObjectDetection/freebie.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ObjectDetection/trick.pdf" type="application/pdf" width="100%" height=1000>
