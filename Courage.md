---
layout: page
title: Courage
---

There are high profile style of courage such as standing up for that is right and what is wrong, such as this Iranian actress.
<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Taraneh Alidoosti: Top Iranian actress poses without headscarf <a href="https://t.co/lstrlb3iyf">https://t.co/lstrlb3iyf</a></p>&mdash; BBC News (World) (@BBCWorld) <a href="https://twitter.com/BBCWorld/status/1590507845963087872?ref_src=twsrc%5Etfw">November 10, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 

But as I grow older, I begin to see courage as something more subtal. I think courage is about being able to strike a balance.

<blockquote class="twitter-tweet"><p lang="zh" dir="ltr">我一直也想让我儿女明白，如果有觉得不舒服的就要说出来，有人欺负或者觉得不公平的就要喊出来而不是默默承受<br>我觉得单纯靠口说教导是没有用的，我也得做一下榜样，不仅从自己做起，也为有勇气为自己受到不公待遇而呐喊的人们鼓掌和给予支持🇨🇦</p>&mdash; Yuwei Luo (@luoyuwei) <a href="https://twitter.com/luoyuwei/status/1495783624729366531?ref_src=twsrc%5Etfw">February 21, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet"><p lang="zh" dir="ltr">21年最满意的成长：Put myself before everything else.<br>毛爹虽好，但我成长环境里的大人们均不甚靠谱，“我得靠谱”是无奈的生存信条。靠谱经常意味要顾全大局、知进能退，thankless；真正靠谱是一种无声润物。<br>感谢21年里亏欠过我的人和事，终于让我习惯置自己于宇宙中心。<br>我指数倍地快乐了，自在了。</p>&mdash; 猫得食儿 (@dmiaww) <a href="https://twitter.com/dmiaww/status/1481413077081812994?ref_src=twsrc%5Etfw">January 12, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 

- It takes courage to hold your tongs. 
- It takes courage to look the other way.
- It takes courage to focus on your path. 