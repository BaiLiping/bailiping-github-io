---
layout: page
title: Matrix Theory
---
- ![1](/assets/docs/Matrix/1.jpg)
- ![1](/assets/docs/Matrix/1.png)

# Book
<embed src="/assets/docs/Matrix/matrixbook.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/Matrix/MatrixComputation.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/Matrix/JohnFrancis.pdf" type="application/pdf" width="100%" height=1000>
# Course Material
## week 1
<embed src="/assets/docs/Matrix/matrix1.pdf" type="application/pdf" width="100%" height=430>

## week 2
<embed src="/assets/docs/Matrix/matrix2.pdf" type="application/pdf" width="100%" height=430>

## week 3
<embed src="/assets/docs/Matrix/matrix3.pdf" type="application/pdf" width="100%" height=430>

## week 4
<embed src="/assets/docs/Matrix/matrix4.pdf" type="application/pdf" width="100%" height=430>

## week 5
<embed src="/assets/docs/Matrix/matrix5.pdf" type="application/pdf" width="100%" height=430>

## week 6
<embed src="/assets/docs/Matrix/matrix6.pdf" type="application/pdf" width="100%" height=430>

## week 7
<embed src="/assets/docs/Matrix/matrix7.pdf" type="application/pdf" width="100%" height=430>

## week 8
<embed src="/assets/docs/Matrix/matrix8.pdf" type="application/pdf" width="100%" height=430>

## week 9
<embed src="/assets/docs/Matrix/matrix9.pdf" type="application/pdf" width="100%" height=430>

## week 10
<embed src="/assets/docs/Matrix/matrix10.pdf" type="application/pdf" width="100%" height=430>
<embed src="/assets/docs/Matrix/assignment1.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/Matrix/assignment2.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/Matrix/assignment3.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/Matrix/assignment4.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/Matrix/assignment5.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/Matrix/assignment6.pdf" type="application/pdf" width="100%" height=1000>