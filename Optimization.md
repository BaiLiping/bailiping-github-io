---
layout: page
title: Optimization
---
# Boyd Material
- [EE236](http://www.seas.ucla.edu/~vandenbe/ee236c.html)
- [EE364a](https://web.stanford.edu/class/ee364a/)
- [EE364b](https://web.stanford.edu/class/ee364b/lectures.html)
# Books
<embed src="/assets/docs/Optimization/Book.pdf" type="application/pdf" width="100%" height=1000>
<embed src="https://stanford.edu/~boyd/cvxbook/bv_cvxbook.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/Optimization/Book1.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/Optimization/problemset.pdf" type="application/pdf" width="100%" height=1000>

# Lecture Recordings
## Convex Optimization II
<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PL7DD2F5ED3D1A1514" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Large Scale Optimization
<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLXsmhnDvpjORzPelSDs0LSDrfJcqyLlZc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PL7DD2F5ED3D1A1514" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# Overview
<embed src="/assets/docs/Optimization/Optimization.pdf" type="application/pdf" width="100%" height=1000>

# Week 1
<iframe src="https://onedrive.live.com/embed?cid=9055BFA70AF97174&amp;resid=9055BFA70AF97174%211256&amp;authkey=AL7F7vf4AFRhEuQ&amp;em=2&amp;wdAr=1.3333333333333333" width="1026px" height="793px" frameborder="0">This is an embedded <a target="_blank" href="https://office.com">Microsoft Office</a> presentation, powered by <a target="_blank" href="https://office.com/webapps">Office</a>.</iframe>

# Week 2
<iframe src="https://onedrive.live.com/embed?cid=9055BFA70AF97174&amp;resid=9055BFA70AF97174%211258&amp;authkey=AEsbajOMpHt0QG4&amp;em=2&amp;wdAr=1.3333333333333333" width="1026px" height="793px" frameborder="0">This is an embedded <a target="_blank" href="https://office.com">Microsoft Office</a> presentation, powered by <a target="_blank" href="https://office.com/webapps">Office</a>.</iframe>

# Week 3
<iframe src="https://onedrive.live.com/embed?cid=9055BFA70AF97174&amp;resid=9055BFA70AF97174%211263&amp;authkey=AGx6nitZLqrtriw&amp;em=2&amp;wdAr=1.3333333333333333" width="1026px" height="793px" frameborder="0">This is an embedded <a target="_blank" href="https://office.com">Microsoft Office</a> presentation, powered by <a target="_blank" href="https://office.com/webapps">Office</a>.</iframe>

# Week 4
<iframe src="https://onedrive.live.com/embed?cid=9055BFA70AF97174&amp;resid=9055BFA70AF97174%211257&amp;authkey=AMKfWc-wWSiGRv0&amp;em=2&amp;wdAr=1.3333333333333333" width="1026px" height="793px" frameborder="0">This is an embedded <a target="_blank" href="https://office.com">Microsoft Office</a> presentation, powered by <a target="_blank" href="https://office.com/webapps">Office</a>.</iframe>

# Week 5
<iframe src="https://onedrive.live.com/embed?cid=9055BFA70AF97174&amp;resid=9055BFA70AF97174%211264&amp;authkey=AMn67nwJtDG9fPE&amp;em=2&amp;wdAr=1.3333333333333333" width="1026px" height="793px" frameborder="0">This is an embedded <a target="_blank" href="https://office.com">Microsoft Office</a> presentation, powered by <a target="_blank" href="https://office.com/webapps">Office</a>.</iframe>

# Week 6
<iframe src="https://onedrive.live.com/embed?cid=9055BFA70AF97174&amp;resid=9055BFA70AF97174%211262&amp;authkey=AHH9V3pnLXlWUY4&amp;em=2&amp;wdAr=1.3333333333333333" width="1026px" height="793px" frameborder="0">This is an embedded <a target="_blank" href="https://office.com">Microsoft Office</a> presentation, powered by <a target="_blank" href="https://office.com/webapps">Office</a>.</iframe>

# Week 7
<iframe src="https://onedrive.live.com/embed?cid=9055BFA70AF97174&amp;resid=9055BFA70AF97174%211266&amp;authkey=AL9a9Bvy_mNTnAY&amp;em=2&amp;wdAr=1.3333333333333333" width="1026px" height="793px" frameborder="0">This is an embedded <a target="_blank" href="https://office.com">Microsoft Office</a> presentation, powered by <a target="_blank" href="https://office.com/webapps">Office</a>.</iframe>

# Week 8
<iframe src="https://onedrive.live.com/embed?cid=9055BFA70AF97174&amp;resid=9055BFA70AF97174%211265&amp;authkey=AJC6EGMDMWquQWI&amp;em=2&amp;wdAr=1.3333333333333333" width="1026px" height="793px" frameborder="0">This is an embedded <a target="_blank" href="https://office.com">Microsoft Office</a> presentation, powered by <a target="_blank" href="https://office.com/webapps">Office</a>.</iframe>

# Week 9
<iframe src="https://onedrive.live.com/embed?cid=9055BFA70AF97174&amp;resid=9055BFA70AF97174%211267&amp;authkey=AGHcnvuTZE5Yq90&amp;em=2&amp;wdAr=1.3333333333333333" width="1026px" height="793px" frameborder="0">This is an embedded <a target="_blank" href="https://office.com">Microsoft Office</a> presentation, powered by <a target="_blank" href="https://office.com/webapps">Office</a>.</iframe>






