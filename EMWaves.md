---
layout: page
title: System
---

----
- TOC
{:toc}
----

### review on EM
- [Greg Durgin](https://www.youtube.com/user/profdurgin/playlists)
- [CEM Lectures](https://www.youtube.com/channel/UCPC6uCfBVSK71MnPPcp8AGA/playlists)

<embed src="/assets/docs/EM/1.pdf" type="application/pdf" width="100%" height=600>
<embed src="/assets/docs/EM/2.pdf" type="application/pdf" width="100%" height=600>
<embed src="/assets/docs/EM/3.pdf" type="application/pdf" width="100%" height=600>
<embed src="/assets/docs/EM/4.pdf" type="application/pdf" width="100%" height=600>
<embed src="/assets/docs/EM/5.pdf" type="application/pdf" width="100%" height=600>
<embed src="/assets/docs/EM/6.pdf" type="application/pdf" width="100%" height=600>
<iframe src="https://onedrive.live.com/embed?cid=9055BFA70AF97174&amp;resid=9055BFA70AF97174%211274&amp;authkey=ALJq5IYnCxJXcv4&amp;em=2&amp;wdAr=1.3333333333333333" width="1026px" height="793px" frameborder="0">This is an embedded <a target="_blank" href="https://office.com">Microsoft Office</a> presentation, powered by <a target="_blank" href="https://office.com/webapps">Office</a>.</iframe>