---
layout: page
title: Radar Signal Processing
---

### important papers
<embed src="/assets/docs/Radar/radar1.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/Radar/radar11.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/Radar/High-Performance_Automotive_Radar_A_Review_of_Signal_Processing_Algorithms_and_Modulation_Schemes.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/Radar/Interference_in_Automotive_Radar_Systems_Characteristics_Mitigation_Techniques_and_Current_and_Future_Research.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/Radar/Automotive_Radar_A_Signal_Processing_Perspective.pdf" type="application/pdf" width="100%" height=430>
<embed src="/assets/docs/Radar/radar5.pdf" type="application/pdf" width="100%" height=430>

<embed src="/assets/docs/Radar/radar2.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/Radar/radar3.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/Radar/radar4.pdf" type="application/pdf" width="100%" height=1000>

<embed src="/assets/docs/Radar/radar6.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/Radar/radar7.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/Radar/radar8.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/Radar/radar9.pdf" type="application/pdf" width="100%" height=1000>
