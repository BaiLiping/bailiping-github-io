---
layout: page
title: Radar Based Tracking
---

----
- TOC
{:toc}
----
### Presentation
<embed src="/assets/docs/Tracking/PHD_final.pdf" type="application/pdf" width="100%" height=480>


### tracking overview

![1](/assets/img/radarproject/projectoverview/MTTBlock.png)
![1](/assets/img/radarproject/projectoverview/tracking.png)
![2](/assets/img/radarproject/filters.png)
![2](/assets/img/radarproject/filters2.png)

### repositories
- [US Naval Research](https://github.com/USNavalResearchLaboratory/TrackerComponentLibrary)
- [Ba Tuong Vo](http://ba-tuong.vo-au.com/codes.html)
- [Stone Soup](https://stonesoup.readthedocs.io/en/latest/index.html)
- [Radar_Perception_Project_2](https://github.com/BaiLiping/Radar_Perception_Project_2)
- [RadarProjectCode](https://github.com/BaiLiping/RadarProjectCode)
- [RadarProjectPaper](https://github.com/BaiLiping/RadarProjectPaper)
- [Ángel García-Fernández(PMBA MATLAB)](https://github.com/Agarciafernandez/MTT)
- [Eric Bohnsack](https://github.com/erikbohnsack/pmbm)

### lecture series on multi-object tracking
- [Tracking Lecture](https://www.control.isy.liu.se/student/graduate/targettracking/)

### important papers

#### mathematical foundation of multiobject tracking
<embed src="/assets/docs/Tracking/overview.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/Tracking/overview2.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/Tracking/xyx.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/Tracking/mahler2003.pdf" type="application/pdf" width="100%" height=1000>

<embed src="/assets/docs/Tracking/compare.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/Tracking/extendedtracking.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/Tracking/advances.pdf" type="application/pdf" width="100%" height=1000>

#### GM-PHD
![1](/assets/img/radarproject/PHD1.PNG)
![1](/assets/img/radarproject/PHD2.PNG)
<embed src="/assets/docs/Tracking/GM-PDA.pdf" type="application/pdf" width="100%" height=1000>


#### JPDA
![1](/assets/img/radarproject/jpda1.png)
![2](/assets/img/radarproject/jpda2.png)
![1](/assets/img/kalman/TrackManagement.png)
![1](/assets/img/kalman/pda.jpg)

<embed src="/assets/docs/Tracking/jpda.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/Tracking/jpda1.pdf" type="application/pdf" width="100%" height=1000>

#### CPHD
<embed src="/assets/docs/Tracking/mahler2007.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/Tracking/cphd2.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/Tracking/cphd.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/Tracking/cphd1.pdf" type="application/pdf" width="100%" height=1000>

#### PMBM
![3](/assets/img/radarproject/pmbm.png)
<embed src="/assets/docs/Tracking/pmbm.pdf" type="application/pdf" width="100%" height=430>
<embed src="/assets/docs/Tracking/PMBM.pdf" type="application/pdf" width="100%" height=430>
<embed src="/assets/docs/Tracking/conjugate_prior.pdf" type="application/pdf" width="100%" height=430>
<embed src="/assets/docs/Tracking/comparison.pdf" type="application/pdf" width="100%" height=1000>

## fusing
![1](/assets/img/radarproject/projectoverview/radarcamerafusing.png)

## statistically derive parameters
<embed src="/assets/docs/Tracking/varingpd.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/Tracking/varingpd2.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/Tracking/p_d.pdf" type="application/pdf" width="100%" height=1000>

## Project Related Materials
<iframe src="https://onedrive.live.com/embed?cid=9055BFA70AF97174&amp;resid=9055BFA70AF97174%211244&amp;authkey=ACYluQ2VpDLndBY&amp;em=2&amp;wdAr=1.3333333333333333" width="722px" height="565px" frameborder="0">This is an embedded <a target="_blank" href="https://office.com">Microsoft Office</a> presentation, powered by <a target="_blank" href="https://office.com/webapps">Office</a>.</iframe>

## Potential Ideas

### MCMC
- <embed src="/assets/docs/Tracking/MCMC.pdf" type="application/pdf" width="100%" height=1000>
- [RL Lab](http://rllab.snu.ac.kr/software/multi-scan-mcmcda)
- [paper](https://ieeexplore.ieee.org/document/4797815)

### Project_5
- [code](https://github.com/BaiLiping/eagerMOT)
- <embed src="/assets/docs/Tracking/eager.pdf" type="application/pdf" width="100%" height=1000>

### IEEE Conference
- [Data Association and Target Tracking](https://us06web.zoom.us/webinar/register/7816147193417/WN_EE1cLnV6TDSt7TxJ3LuaLA)






