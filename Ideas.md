---
layout: page
title: Ideas
---
<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Drone formation control can probably be utilized to create decoy RCS. One challenging scenario for multi-object tracking is parallel/intersecting tracks. Formation control + cleverly designed trajectories probably can go a long way in terms of deception of the enemy radar. <a href="https://t.co/wto6TTeShT">https://t.co/wto6TTeShT</a></p>&mdash; Bai Liping (白莉萍) (@bai_liping) <a href="https://twitter.com/bai_liping/status/1591391218218700801?ref_src=twsrc%5Etfw">November 12, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">That is what it looks like<a href="https://t.co/jhnE6QEvUZ">https://t.co/jhnE6QEvUZ</a></p>&mdash; Bai Liping (白莉萍) (@bai_liping) <a href="https://twitter.com/bai_liping/status/1544187942842683392?ref_src=twsrc%5Etfw">July 5, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 

<blockquote class="twitter-tweet"><p lang="zh" dir="ltr">AI看相 <a href="https://t.co/ShX4G4oVsq">https://t.co/ShX4G4oVsq</a> 那么多年高考人脸和分数数据应该可以训练一个网络，直接看人脸估分。</p>&mdash; Bai Liping (白莉萍) (@bai_liping) <a href="https://twitter.com/bai_liping/status/1280470625669148672?ref_src=twsrc%5Etfw">July 7, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 


[1] **Human as an intricate mechanical computer**. Kinase is a group of enzyme that phosphorylation protein. With phosphorylation, protein would deform,  hence a programmable mechanical cog. On the programming side of the story, metalization of DNA controls what to transcribe. For instance, when the environment head up, certain part of the DNA is metalized and different set of protein, known as heatshock protein is transcribed to perform protective function such that other proteins would experience less deformation. **Is there a language for programming this mechanical computer**?

[2] **Relative Information**. I think information is inherently relative, this might hold the key to decision making under uncertainty.

[3] **State of Brain**. How many states brain have? Why do we change from one state to another? Can we learn about brain through the methods developed in other discipline such as statistical mechanics. There are certain set of macro parameters which can describe the microstate, much like state transition in physics, there is a state transition in brain function as well.

[4] **RL for things that are dynamic**. Now the set up of RL assume MDP that is static. What would happen if that assumption is invalid such as trading, where the system actually changes over time.  

[5] **Multi-Agent Reinforcement Learning for Trading** my understanding is that financial system offload risk from those who do things to those who have the means to absorb those risks. Of course there is a kink in that logic, since most of the time the risk is not taken upon by those who is most suited, but those who is least informed. But here, let us suppose that the financial system is a perfect system and the risks goes to those who can bear them. The portfolio should consists of all kinds of liquid assets, and for each asset, there is an agent who makes the long/short decision. When all those agents works together, you have a real all-weather portfolio, which can be adjusted according to the economics circumstances.
