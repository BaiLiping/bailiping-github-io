---
layout: page
title: Control
---

# Supplimentary Material
- [Data Driven Science&Engineering](http://www.databookuw.com/)
- [UnderActuated Robotics](https://underactuated.mit.edu/)
- [Advanced Robotics](https://people.eecs.berkeley.edu/~pabbeel/cs287-fa19/)
- [L4DC](https://l4dc.ethz.ch/)
