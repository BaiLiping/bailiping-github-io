---
layout: page
title: Seminal Papers
---
[Pieter Abbeel](https://people.eecs.berkeley.edu/~pabbeel/cs287-fa09/)

<embed src="/assets/docs/ReadingList/1.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ReadingList/2.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ReadingList/3.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ReadingList/4.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ReadingList/5.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ReadingList/6.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ReadingList/7.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ReadingList/8.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ReadingList/9.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ReadingList/10.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ReadingList/11.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ReadingList/12.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ReadingList/13.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ReadingList/14.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ReadingList/15.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ReadingList/16.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ReadingList/17.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ReadingList/18.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ReadingList/19.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ReadingList/20.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ReadingList/21.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ReadingList/22.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ReadingList/23.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ReadingList/24.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ReadingList/25.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ReadingList/26.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ReadingList/27.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ReadingList/28.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ReadingList/29.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ReadingList/30.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ReadingList/31.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ReadingList/33.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ReadingList/34.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ReadingList/35.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ReadingList/36.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ReadingList/37.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ReadingList/38.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ReadingList/40.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ReadingList/41.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/ReadingList/42.pdf" type="application/pdf" width="100%" height=1000>
