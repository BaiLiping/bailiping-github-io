---
layout: post
title: Robust Control
tags: control
image:
show-avatar: false
social-share: false
comments: true
---
- <embed src="/assets/docs/RobustControl/robustcontrol.pdf" type="application/pdf" width="100%" height=1000>
- <embed src="/assets/docs/RobustControl/robustcontrol1.pdf" type="application/pdf" width="100%" height=1000>
- <embed src="/assets/docs/RobustControl/robustcontrol2.pdf" type="application/pdf" width="100%" height=1000>
- <embed src="/assets/docs/RobustControl/OptimalControlandEstimation.pdf" type="application/pdf" width="100%" height=1000>

