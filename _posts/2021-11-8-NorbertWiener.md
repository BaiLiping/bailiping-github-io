---
layout: post
title: Norbert Wiener
tags: reading
image:
show-avatar: false
social-share: false
comments: true
---
<embed src="/assets/docs/Books/Cybernetics, or Control and Communication in the Animal and the Machine by Norbert Wiener.pdf" type="application/pdf" width="100%" height=1000>
<embed src="/assets/docs/Books/The Human Use of Human Beings by Norbert Wiener.pdf" type="application/pdf" width="100%" height=1000>

