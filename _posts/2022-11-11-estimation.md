---
layout: post
title: Estimation Theory
tags: record
image:
show-avatar: false
social-share: false
comments: true
---

- <embed src="/assets/docs/Estimation/StevenKay1.pdf" type="application/pdf" width="100%" height=1000>
- <embed src="/assets/docs/Estimation/StevenKay2.pdf" type="application/pdf" width="100%" height=1000>

![1](/assets/img/estimation/compare.png)
